<?php
include ('user/login.php');
if (isset($_SESSION['userid'])){
    header('Location: home.php');
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Shareables</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="css/style.min.css" rel="stylesheet">

</head>

<body>

<!-- Navbar -->
<nav class="navbar fixed-top navbar-expand-lg navbar-dark scrolling-navbar">
    <div class="container">

        <!-- Brand -->
        <a class="navbar-brand" href="index.php"><strong>Shareables</strong></a>

        <!-- Collapse -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Links -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left -->
            <ul class="navbar-nav mr-auto">
            </ul>
            <!-- Right -->
            <ul class="navbar-nav nav-flex-icons">
                <li class="nav-item">
                    <a href="about_us.php" class="nav-link border border-light rounded">
                        <i class="fas fa-user-astronaut"></i> About us
                    </a>
                </li>
                <li class="nav-item">
                    <a href="https://gitlab.com/studibrudis2020/shareables" class="nav-link border border-light rounded"
                       target="_blank">
                        <i class="fab fa-gitlab mr-2"></i>Shareables GitLab
                    </a>
                </li>
            </ul>

        </div>

    </div>
</nav>
<!-- Navbar -->

<!-- Full Page Intro -->
<div class="view full-page-intro" style="background-image: url('img/fullpageintro.jpg'); background-repeat: no-repeat; background-size: cover;">

    <!-- Mask & flexbox options-->
    <div class="mask rgba-black-light d-flex justify-content-center align-items-center">

        <!-- Content -->
        <div class="container">

            <!--Grid row-->
            <div class="row wow fadeIn">

                <!--Grid column-->
                <div class="col-md-6 mb-4 white-text text-center text-md-left">

                    <h1 class="display-4 font-weight-bold">All your accounts in one place</h1>

                    <hr class="hr-light">

                    <p>
                        <strong>We offer a wide varity of streaming and gaming accounts</strong>
                    </p>

                    <p class="mb-4 d-none d-md-block">
                        <strong>
                            Here on SHAREABLES you will find many accounts like Spotify, Netflix, Amazon Prime, etc. and
                            all of them are <strong>free</strong> of charge.
                        </strong>
                    </p>

                </div>
                <!--Grid column-->

                <!--Grid column-->
                <div class="col-md-6 col-xl-5 mb-4">

                    <!--Card-->
                    <div class="card">

                        <!--Card content-->
                        <div class="card-body">

                            <!-- Form -->
                            <form name="login" action="?login" method="post">
                                <!-- Heading -->
                                <h3 class="dark-grey-text text-center">
                                    <strong>Login</strong>
                                </h3>
                                <hr>
                                <div class="md-form">
                                    <i class="fas fa-user prefix grey-text"></i>
                                    <input type="email" id="email" name="email" class="form-control">
                                    <label for="email">E-Mail</label>
                                </div>
                                <div class="md-form">
                                    <i class="fas fa-envelope prefix grey-text"></i>
                                    <input type="password" id="passwort" name="passwort" class="form-control">
                                    <label for="passwort">Password</label>
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-indigo">Login</button>
                                    <hr>
                                    <fieldset class="">
                                        Not registered? <a href="signup.php">Create an account</a>
                                    </fieldset>
                                </div>

                            </form>
                            <!-- Form -->

                        </div>

                    </div>
                    <!--/.Card-->

                </div>
                <!--Grid column-->

            </div>
            <!--Grid row-->

        </div>
        <!-- Content -->

    </div>
    <!-- Mask & flexbox options-->

</div>
<!-- Full Page Intro -->

<!--Main layout-->
<main>
    <div class="container">

        <!--Section: Main info-->
        <section class="mt-5 wow fadeIn">

            <!--Grid row-->
            <div class="row">

                <!--Grid column-->
                <div class="col-md-6 mb-4">

                    <img src="img/landingpage1.jpg" class="img-fluid z-depth-1-half" alt="Old School Electronics">

                </div>
                <!--Grid column-->

                <!--Grid column-->
                <div class="col-md-6 mb-4">

                    <!-- Main heading -->
                    <h3 class="h3 mb-3">Begone Boredom</h3>
                    <p>We at SHAREABLES created this service, so <strong>you</strong> don't need to be bored anymore.</p>
                    <p>Create an account today and experience games, music and movies without spending any money.</p>
                    <!-- Main heading -->

                    <hr>

                    <p>Our database is being expanded everyday so new users don't have to miss out on the fun!</p>

                    <!-- CTA -->

                </div>
                <!--Grid column-->

            </div>
            <!--Grid row-->

        </section>
        <!--Section: Main info-->

        <hr class="my-5">

        <!--Section: Not enough-->
        <section>

            <h2 class="my-5 h3 text-center">You want it? You get it!</h2>

            <!--First row-->
            <div class="row features-small mb-5 mt-3 wow fadeIn">

                <!--First column-->
                <div class="col-md-4">
                    <!--First row-->
                    <div class="row">
                        <div class="col-2">
                            <i class="fas fa-headphones fa-2x indigo-text"></i>
                        </div>
                        <div class="col-10">
                            <h6 class="feature-title">Spotify</h6>
                            <p class="grey-text">Are your neighbours too loud again? Now you can be too! Blast your favorite artists on full volume with <strong>Spotify</strong>!
                            </p>
                            <div style="height:15px"></div>
                        </div>
                    </div>
                    <!--/First row-->

                    <!--Second row-->
                    <div class="row">
                        <div class="col-2">
                            <i class="fas fa-film fa-2x indigo-text"></i>
                        </div>
                        <div class="col-10">
                            <h6 class="feature-title">Netflix</h6>
                            <p class="grey-text">Wanna' movies and chill? Oh wait, it's called "<strong>Netflix</strong> and Chill"!

                            </p>
                            <div style="height:15px"></div>
                        </div>
                    </div>
                    <!--/Second row-->

                    <!--Third row-->
                    <div class="row">
                        <div class="col-2">
                            <i class="fas fa-film fa-2x indigo-text"></i>
                        </div>
                        <div class="col-10">
                            <h6 class="feature-title">Prime Video</h6>
                            <p class="grey-text">Do you think that you only need one video-streaming service? You are wrong! There's <strong>Prime Video</strong> too!</p>
                            <div style="height:15px"></div>
                        </div>
                    </div>
                    <!--/Third row-->
                </div>
                <!--/First column-->

                <!--Second column-->
                <div class="col-md-4 flex-center">
                    <img src="img/landingpage2.png" class="z-depth-0 img-fluid" alt="Various Logos">
                </div>
                <!--/Second column-->

                <!--Third column-->
                <div class="col-md-4 mt-2">
                    <!--First row-->
                    <div class="row">
                        <div class="col-2">
                            <i class="fas fa-gamepad fa-2x indigo-text"></i>
                        </div>
                        <div class="col-10">
                            <h6 class="feature-title">Epic Games</h6>
                            <p class="grey-text">Well, it ain't Steam, but it got a lot of exclusive video-game titles!
                            </p>
                            <div style="height:15px"></div>
                        </div>
                    </div>
                    <!--/First row-->

                    <!--Second row-->
                    <div class="row">
                        <div class="col-2">
                            <i class="fas fa-gamepad fa-2x indigo-text"></i>
                        </div>
                        <div class="col-10">
                            <h6 class="feature-title">Origin</h6>
                            <p class="grey-text">You want to play some good ol' 'Battlefield'? You can, bust just on Origin! Thanks for nothng EA!</p>
                            <div style="height:15px"></div>
                        </div>
                    </div>
                    <!--/Second row-->

                    <!--Third row-->
                    <div class="row">
                        <div class="col-2">
                            <i class="fas fa-gamepad fa-2x indigo-text"></i>
                        </div>
                        <div class="col-10">
                            <h6 class="feature-title">Steam</h6>
                            <p class="grey-text">Now this is it. This is the pinnacle of existence! Steam! It's all you need, it's all you want for gaming!
                            </p>
                            <div style="height:15px"></div>
                        </div>
                    </div>
                    <!--/Third row-->
                </div>
                <!--/Third column-->

            </div>
            <!--/First row-->

        </section>
        <!--Section: Not enough-->
    </div>
</main>
<!--Main layout-->

<!--Footer-->
<footer class="page-footer text-center font-small mt-4 wow fadeIn">

    <hr class="my-4">

    <!-- Social icons -->
    <div class="pb-4">
        <a href="about_us.php">
          <i class="fas fa-user-astronaut"></i>
        </a>
        <a href="https://gitlab.com/studibrudis2020/shareables" target="_blank">
            <i class="fab fa-gitlab mr-2"></i>
        </a>

    </div>
    <!-- Social icons -->

    <!--Copyright-->
    <div class="footer-copyright py-3">
        © 2020 Copyright:
        <a href="https://mdbootstrap.com" target="_blank"> MDBootstrap.com </a>
    </div>
    <!--/.Copyright-->

</footer>
<!--/.Footer-->
<!-- SCRIPTS -->
<!-- JQuery -->
<script src="js/jquery-3.4.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script src="js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script src="js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script src="js/mdb.min.js"></script>
<!-- Initializations -->
<script>
    // Animations initialization
    new WOW().init();
</script>
</body>

</html>