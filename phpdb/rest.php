<?php
include 'admin_proof.php';  // Used for Session-ID

// Allow every Webpage to use the service
header("Access-Control-Allow-Origin: *");
// Defines the responding Content-Type
header("Content-Type: application/json; charset=UTF-8");
// Defines the supported Methods
header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");

//open connection to mysql db
$connection = mysqli_connect("localhost","shareables","share123","shareables") or die("Error " . mysqli_error($connection));

// Save the requested method in a variable (GET, POST, PUT, DELETE, ...)
$method = $_SERVER['REQUEST_METHOD'];

// save the body of the request in der variable $payload
$payload = file_get_contents('php://input');

switch ($method) {
    case 'GET':
        if (isset($_GET['info'])) {
            $sql = "SELECT * FROM accounts";
            $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));//create an array
            $temp = array();
            while ($row = mysqli_fetch_assoc($result)) {
                $temp[] = $row;
            }
            echo json_encode(array_filter($temp));
            http_response_code(200);
        }
        if(isset($_GET['accounts'])){
            $sql = "SELECT products.name, usermail, username, accounts.passwort FROM accounts JOIN users, products WHERE users.id = '".$id."' AND accounts.email = users.email AND accounts.pid = products.pid ORDER BY products.name";
            $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

            $temp = array();
            while($row = mysqli_fetch_assoc($result))
            {
                $temp[] = $row;
            }

            echo json_encode(array_filter($temp));
            http_response_code(200);
        }

        break;
    case 'POST':
        // Post is for creating new resources
        if(isset($_GET['insert'])) {
            $error = false;
            $usermail = $_POST['usermail1'];
            $username = $_POST['username1'];
            $passwort = $_POST['passwort1'];
            $pid = $_POST['pid1'];

            if(empty($usermail) and empty($username)){
                echo '<script language="javascript">alert("The account must have a usermail or username!")</script>';
                header('Refresh: 0; url=../info.php');
                http_response_code(205);
            } else if(empty($usermail)){
                $sql = "INSERT INTO accounts(username, passwort, pid) VALUES ('".$username."', '".$passwort."', '".$pid."')";
                mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
                header('Refresh: 0; url=../info.php');
                http_response_code(201);
            } else if(empty($username)){
                $sql = "INSERT INTO accounts(usermail, passwort, pid) VALUES ('".$usermail."', '".$passwort."', '".$pid."')";
                mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
                header('Refresh: 0; url=../info.php');
                http_response_code(201);
            } else if(!empty($usermail) and !empty($username)){
                $sql = "INSERT INTO accounts(usermail, username, passwort, pid) VALUES ('".$usermail."', '".$username."', '".$passwort."', '".$pid."')";
                mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
                header('Refresh: 0; url=../info.php');
                http_response_code(201);
            } else {
                echo '<script language="javascript">alert("An error has occured!")</script>';
                header('Refresh: 0; url=../info.php');
                http_response_code(205);
            }
        }

        break;
    case 'PUT':
        // update/replace resource if the id does exist (Statuscode 200)
        if(isset($_GET['replace'])) {
            parse_str(file_get_contents("php://input"), $_PUT);
            foreach ($_PUT as $key => $value)
            {
                echo $value;
                unset($_PUT[$key]);

                $_PUT[str_replace('amp;', '', $key)] = $value;
            }

            $_REQUEST = array_merge($_REQUEST, $_PUT);

            $aid = $_REQUEST['aid'];
            $usermail = $_REQUEST['usermail2'];
            $username = $_REQUEST['username2'];
            $passwort = $_REQUEST['passwort2'];
            $pid = $_REQUEST['pid2'];
            $email = $_REQUEST['email'];

            if(empty($usermail) and empty($username)){
                echo '<script language="javascript">alert("The account must have a usermail or username!")</script>';
                header('Refresh: 0; url=../info.php');
                http_response_code(205);
            } else if(empty($usermail)){
                if(empty($email)){
                    $sql1 = "DELETE FROM accounts WHERE aid = '".$aid."'";
                    mysqli_query($connection, $sql1) or die("Error in Selecting " . mysqli_error($connection));

                    $sql2 = "INSERT INTO accounts(aid, username, passwort, pid) VALUES ('".$aid."', '".$username."', '".$passwort."', '".$pid."')";
                    mysqli_query($connection, $sql2) or die("Error in Selecting " . mysqli_error($connection));
                    http_response_code(201);
                } else {
                    $sql1 = "DELETE FROM accounts WHERE aid = '".$aid."'";
                    mysqli_query($connection, $sql1) or die("Error in Selecting " . mysqli_error($connection));

                    $sql2 = "INSERT INTO accounts(aid, username, passwort, pid, email) VALUES ('".$aid."', '".$username."', '".$passwort."', '".$pid."', '".$email."')";
                    mysqli_query($connection, $sql2) or die("Error in Selecting " . mysqli_error($connection));
                    http_response_code(201);
                }
                header('Refresh: 0; url=../info.php');
            } else if(empty($username)){
                if(empty($email)){
                    $sql1 = "DELETE FROM accounts WHERE aid = '".$aid."'";
                    mysqli_query($connection, $sql1) or die("Error in Selecting " . mysqli_error($connection));

                    $sql2 = "INSERT INTO accounts(aid, usermail, passwort, pid) VALUES ('".$aid."', '".$usermail."', '".$passwort."', '".$pid."')";
                    mysqli_query($connection, $sql2) or die("Error in Selecting " . mysqli_error($connection));
                    http_response_code(201);
                } else {
                    $sql1 = "DELETE FROM accounts WHERE aid = '".$aid."'";
                    mysqli_query($connection, $sql1) or die("Error in Selecting " . mysqli_error($connection));

                    $sql2 = "INSERT INTO accounts(aid, usermail, passwort, pid, email) VALUES ('".$aid."', '".$usermail."', '".$passwort."', '".$pid."', '".$email."')";
                    mysqli_query($connection, $sql2) or die("Error in Selecting " . mysqli_error($connection));
                    http_response_code(201);
                }
                header('Refresh: 0; url=../info.php');
            } else if(!empty($usermail) and !empty($username)){
                if(empty($email)){
                    $sql1 = "DELETE FROM accounts WHERE aid = '".$aid."'";
                    mysqli_query($connection, $sql1) or die("Error in Selecting " . mysqli_error($connection));

                    $sql2 = "INSERT INTO accounts(aid, usermail, username, passwort, pid) VALUES ('".$aid."', '".$usermail."', '".$username."', '".$passwort."', '".$pid."')";
                    mysqli_query($connection, $sql2) or die("Error in Selecting " . mysqli_error($connection));
                    http_response_code(201);
                } else {
                    $sql1 = "DELETE FROM accounts WHERE aid = '".$aid."'";
                    mysqli_query($connection, $sql1) or die("Error in Selecting " . mysqli_error($connection));

                    $sql2 = "INSERT INTO accounts(aid, usermail, username, passwort, pid, email) VALUES ('".$aid."', '".$usermail."', '".$username."', '".$passwort."', '".$pid."', '".$email."')";
                    mysqli_query($connection, $sql2) or die("Error in Selecting " . mysqli_error($connection));
                    http_response_code(201);
                }
                header('Refresh: 0; url=../info.php');
            } else {
                echo '<script language="javascript">alert("An error has occured!")</script>';
                header('Refresh: 0; url=../info.php');
                http_response_code(205);
            }
        }

        break;
    case 'DELETE':
        // delete resource if the id does exist (Statuscode 200)
        // error (Statuscode 404) if the resource does not exist

        if(isset($_GET['delete'])) {
            $url = $_SERVER['REQUEST_URI'];
            $aid = array_slice(explode('=', $url), -1)[0];
            $sql = "DELETE FROM accounts WHERE aid = '" . $aid . "'";
            mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

            http_response_code(200);
        } else {
            echo '<script language="javascript">alert("An error has occured!")</script>';
            http_response_code(404);
        }

        break;
    default :
        // Method not implemented
        http_response_code(501);
}
?>