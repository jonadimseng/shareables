<?php
$mysqli = new mysqli("localhost", "shareables", "share123", "shareables");
if ($mysqli->connect_errno) {
    echo "Connect failed ".$mysqli->connect_error;
    exit();
}
$query = 'SELECT aid, usermail, username, passwort, pid FROM accounts';
$accountsArr = array();
if ($result = $mysqli->query($query)) {
    while ($row = $result->fetch_assoc()) {
        array_push($accountsArr, $row);
    }

    if(count($accountsArr)){
        createXMLfile($accountsArr);
    }
    $result->free();
}
$mysqli->close();

function createXMLfile($accountsArr){

    $filePath = 'accounts.xml';
    $dom     = new DOMDocument('1.0', 'utf-8');
    $root      = $dom->createElement('xml');
    for($i=0; $i<count($accountsArr); $i++){

        $accountID     =  $accountsArr[$i]['aid'];
        $accountMail      =  $accountsArr[$i]['usermail'];
        $accountName    =  $accountsArr[$i]['username'];
        $accountPw     =  $accountsArr[$i]['passwort'];
        $productID      =  $accountsArr[$i]['pid'];
        $account = $dom->createElement('account');
        $account->setAttribute('aid', $accountID);
        $accMail     = $dom->createElement('usermail', $accountMail);
        $account->appendChild($accMail);
        $accName   = $dom->createElement('username', $accountName);
        $account->appendChild($accName);
        $accPwd    = $dom->createElement('passwort', $accountPw);
        $account->appendChild($accPwd);
        $pid     = $dom->createElement('pid', $productID);
        $account->appendChild($pid);
        $root->appendChild($account);
    }
    $dom->appendChild($root);
    $dom->save($filePath);
}