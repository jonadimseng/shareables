<?php
session_start();
$pdo = new PDO('mysql:host=localhost;dbname=shareables', 'shareables', 'share123');

$id = $_SESSION['userid'];

$email = $pdo->prepare("SELECT email FROM users WHERE id = :id");
$email->execute(array('id' => $id));
$emailc = $email->fetch();

//echo $emailc['email'];

$pid = $pdo->prepare("SELECT pid FROM products WHERE name = :name");
$pid->execute(array('name' => $name));
$pidc = $pid->fetch();

//echo $pidc['pid'];

$exists = $pdo->prepare("SELECT email FROM accounts WHERE pid = :pid AND email = :email");
$exists->execute(array('pid' => $pidc['pid'], 'email' => $emailc['email']));
$existsc = $exists->fetch();

$noacc = $pdo->prepare("SELECT COUNT(*) AS anzahl FROM accounts WHERE pid = :pid AND email IS NULL");
$noacc->execute(array('pid' => $pidc['pid']));
$noaccc = $noacc->fetch();

if($existsc){
    echo '<script language="javascript">alert("You already have an account!")</script>';
    header('Refresh: 0; url=../accounts.php');
} else if($noaccc['anzahl'] == 0){
    echo '<script language="javascript">alert("We have no accounts left!")</script>';
    header('Refresh: 0; url=../home.php');
}
else{
    $getAccount = $pdo->prepare("UPDATE accounts SET email = :email WHERE pid = :pid AND email IS NULL LIMIT 1");
    $result = $getAccount->execute(array('email' => $emailc['email'], 'pid' => $pidc['pid']));

    if($result){
        echo '<script language="javascript">alert("Now you have an account :)")</script>';
        header('Location: ../accounts.php');
    }
}