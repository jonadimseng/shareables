<?php
include 'phpdb/admin_proof.php';
include 'user/geheim.php';
if (isset($_POST["logout"])) {
    include 'user/logout.php';
}
include 'phpdb/user_accounts.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>My Accounts</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="css/style.min.css" rel="stylesheet">

    <link href="css/main.css" rel="stylesheet">

    <script src="js/accounts.js"></script>
</head>

<body>

<!--Main Navigation-->
<header>

    <!-- Navbar -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
        <div class="container">

            <!-- Brand -->
            <a class="navbar-brand waves-effect" href="index.php">
                <strong class="blue-text">Shareables</strong>
            </a>

            <!-- Collapse -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <!-- Links -->
            <div class="collapse navbar-collapse" id="navbarSupportedContent">

                <!-- Left -->
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link waves-effect" href="home.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link waves-effect" href="search.php">Search</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link waves-effect" href="accounts.php">Your Accounts</a>
                    </li>
                    <?php
                    if ($emailc['email'] == "admin@shareables.at") { ?>
                        <li class="nav-item">
                            <a class="nav-link waves-effect" href="info.php">Admin Info</a>
                        </li>
                    <?php } ?>
                </ul>

                <!-- Right -->
                <ul class="navbar-nav nav-flex-icons">
                    <li class="nav-item">
                        <a href="https://gitlab.com/studibrudis2020/shareables" class="nav-link waves-effect"
                           target="_blank">
                            <i class="fab fa-gitlab mr-2"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <form action="?logout" method="post">
                            <button class="nav-link border border-light rounded waves-effect" type="submit"
                                    name="logout">
                                <i class="fas fa-sign-out-alt"></i>Logout
                            </button>
                        </form>
                    </li>
                </ul>

            </div>

        </div>
    </nav>
    <!-- Navbar -->

</header>
<!--Main Navigation-->

<!--Main layout-->
<main class="mt-5 pt-5">
    <div class="container">

        <!--Section: Jumbotron-->
        <section class="card wow fadeIn" id="fancybackground">

            <!-- Content -->
            <div class="card-body text-white text-center py-5 px-5 my-5">
                <h1 class="mb-4">
                    <strong>ALL YOUR ACCOUNTS, IN ONE PLACE!</strong>
                </h1>

            </div>
            <!-- Content -->
        </section>
        <!--Section: Jumbotron-->

        <hr class="my-5">

        <!--Section: Cards-->
        <section>

            <div class="container-fluid">
                <div class="table-responsive text-nowrap">
                    <h4 class="card-title">My Accounts</h4>
                    <table class="table" id="accounts">
                        <tr>
                            <th>Platform</th>
                            <th>E-Mail</th>
                            <th>Username</th>
                            <th>Password</th>
                        </tr>
                    </table>
                </div>
            </div>

        </section>
        <!--Section: Cards-->

    </div>
</main>
<!--Main layout-->

<!--Footer-->
<footer class="page-footer text-center font-small mt-4 wow fadeIn">

    <hr class="my-4">

    <!-- Social icons -->
    <div class="pb-4">
        <a href="about_us.php">
            <i class="fas fa-user-astronaut"></i>
        </a>
        &nbsp;&nbsp;
        <a href="https://gitlab.com/studibrudis2020/shareables" target="_blank">
            <i class="fab fa-gitlab mr-2"></i>
        </a>

    </div>
    <!-- Social icons -->

    <!--Copyright-->
    <div class="footer-copyright py-3">
        © 2020 Copyright:
        <a href="https://mdbootstrap.com" target="_blank"> MDBootstrap.com </a>
    </div>
    <!--/.Copyright-->

</footer>
<!--/.Footer-->

<!-- SCRIPTS -->
<!-- JQuery -->
<script src="js/jquery-3.4.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script src="js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script src="js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script src="js/mdb.min.js"></script>
<!-- Initializations -->
<script>
    // Animations initialization
    new WOW().init();
</script>
<script src="js/covid.js"></script>
</body>

</html>