document.addEventListener("DOMContentLoaded", function (event) {
    class AccountClass {
        constructor(productName, accountMail, accountName, accountPassword) {
            this.name = productName;
            this.usermail = accountMail;
            this.username = accountName;
            this.passwort = accountPassword
        }

        addAccountToList() {
            const root = document.getElementById("accounts");
            let rootrow = document.createElement("tr")
            rootrow.setAttribute("id", "account_name_"+this.name);
            rootrow.setAttribute("class", "accountrows");

            let tdPNAME = document.createElement("td");
            let PNAME = document.createTextNode(this.name);
            tdPNAME.appendChild(PNAME);

            let tdAMAIL = document.createElement("td");
            if (this.usermail == null){
                let AMAIL = document.createTextNode("---");
                tdAMAIL.appendChild(AMAIL);
            } else {
                let AMAIL = document.createTextNode(this.usermail);
                tdAMAIL.appendChild(AMAIL);
            }

            let tdANAME = document.createElement("td");
            if (this.username == null){
                let ANAME = document.createTextNode("---");
                tdANAME.appendChild(ANAME);
            } else {
                let ANAME = document.createTextNode(this.username);
                tdANAME.appendChild(ANAME);
            }

            let tdAPWD = document.createElement("td");
            let APWD = document.createTextNode(this.passwort);
            tdAPWD.appendChild(APWD);

            rootrow.appendChild(tdPNAME);
            rootrow.appendChild(tdAMAIL);
            rootrow.appendChild(tdANAME);
            rootrow.appendChild(tdAPWD);
            root.appendChild(rootrow);
        }
    }

    //Execute everything here
    let accounts = [];
    loadAccounts()
    //to here

    function loadAccounts() {
        let getxhttp = new XMLHttpRequest();
        getxhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                let jsondata = JSON.parse(this.responseText);
                for (let i in jsondata){
                    accounts[i] = new AccountClass();
                    Object.assign(accounts[i], jsondata[i]);
                    accounts[i].addAccountToList()
                }
            }
        };
        getxhttp.open("GET", "phpdb/rest.php?accounts",true);
        getxhttp.send();
    }
});