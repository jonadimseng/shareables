let nav = document.getElementsByTagName("ul")[0];
let node = document.createElement("li");
node.setAttribute("class", "nav-item");
let link = document.createElement("a");
link.setAttribute("class", "nav-link waves-effect");
link.setAttribute("href", "https://www.who.int/emergencies/diseases/novel-coronavirus-2019");
link.setAttribute("target", "_blank");
link.setAttribute("id", "covid");
let text = document.createTextNode("loading ...");

let req = new XMLHttpRequest();
req.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
        let info = JSON.parse(this.responseText).data;
        let report = "<strong>"+"COVID-19 Stats: Total Cases: " + info.total_cases + "; Recovery Cases: " + info.recovery_cases +"</strong>";
        document.getElementById("covid").innerHTML = report;
    }
};
req.open('GET', 'https://corona-virus-stats.herokuapp.com/api/v1/cases/general-stats', true);
req.send();

link.appendChild(text);
node.appendChild(link);
nav.appendChild(node);