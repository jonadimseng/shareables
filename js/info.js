document.addEventListener("DOMContentLoaded", function (event) {
    class AccountClass {
        constructor(accountID, accountMail, accountName, accountPassword, productID, userEmail) {
            this.aid = accountID;
            this.usermail = accountMail;
            this.username = accountName;
            this.passwort = accountPassword
            this.pid = productID;
            this.email = userEmail;
        }

        addAccountToList() {
            const root = document.getElementById("infoaccounts");
            let rootrow = document.createElement("tr")
            rootrow.setAttribute("id", "account_nr_"+this.aid);
            rootrow.setAttribute("class", "accountrows");

            let tdAID = document.createElement("td");
            let AID = document.createTextNode(this.aid);
            tdAID.appendChild(AID);

            let tdAMAIL = document.createElement("td");
            if (this.usermail == null){
                let AMAIL = document.createTextNode("---");
                tdAMAIL.appendChild(AMAIL);
            } else {
                let AMAIL = document.createTextNode(this.usermail);
                tdAMAIL.appendChild(AMAIL);
            }

            let tdANAME = document.createElement("td");
            if (this.username == null){
                let ANAME = document.createTextNode("---");
                tdANAME.appendChild(ANAME);
            } else {
                let ANAME = document.createTextNode(this.username);
                tdANAME.appendChild(ANAME);
            }

            let tdAPWD = document.createElement("td");
            let APWD = document.createTextNode(this.passwort);
            tdAPWD.appendChild(APWD);

            let tdPID = document.createElement("td");
            let PID = document.createTextNode(this.pid);
            tdPID.appendChild(PID);

            let tdUMAIL = document.createElement("td");
            if (this.email == null){
                let UMAIL = document.createTextNode("---");
                tdUMAIL.appendChild(UMAIL);
            } else {
                let UMAIL = document.createTextNode(this.email);
                tdUMAIL.appendChild(UMAIL);
            }

            let tdBUTTON = document.createElement("td");
            let delbtn = document.createElement("button");
            delbtn.setAttribute("class", "deleteAccount");
            let icon = document.createElement("i")
            icon.setAttribute("class", "fas fa-trash")
            delbtn.appendChild(icon);
            tdBUTTON.appendChild(delbtn);

            rootrow.appendChild(tdAID);
            rootrow.appendChild(tdAMAIL);
            rootrow.appendChild(tdANAME);
            rootrow.appendChild(tdAPWD);
            rootrow.appendChild(tdPID);
            rootrow.appendChild(tdUMAIL);
            rootrow.appendChild(tdBUTTON)
            root.appendChild(rootrow);
        }

        deleteAccountFromDB(){
            let deletexhttp = new XMLHttpRequest();
            deletexhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    //TODO: MAKE THIS WORK
                    document.getElementById("account_nr_"+this.aid).remove()
                }
            };
            deletexhttp.open("DELETE", "phpdb/rest.php?delete="+this.aid, true);
            deletexhttp.send();

            //TODO: NOT THIS
            document.getElementById("account_nr_"+this.aid).remove()
            generateReplaceID()
        }
    }

    //Execute everything here
    let accounts = [];
    loadAccounts()
    loadListenerPost()
    loadListenerPut()

    function loadListenerDelete() {
        let delbtns = document.getElementsByClassName("deleteAccount");
        for (let i = 0; i < delbtns.length; i++) {
            delbtns[i].addEventListener("click", function () {
                setTimeout(function(){ setAccountAmount(); }, 200);
                accounts[i].deleteAccountFromDB()
            });
        }
        const buttons = document.querySelectorAll('.deleteAccount')
        buttons.forEach(function(currentBtn){
            currentBtn.setAttribute("class", "nav-link border border-light rounded waves-effect")
        })
    }

    function loadListenerPost() {
        const postbtn = document.getElementById("insertbtn");
        postbtn.addEventListener("click", function () {
            let form = document.getElementById("insert")
            let postxhttp = new XMLHttpRequest();
            postxhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 201) {
                    unloadAccounts();
                    loadAccounts()
                    form.reset();
                }
            };
            postxhttp.open("POST", "phpdb/rest.php?insert", true);
            postxhttp.send(new FormData(form));
        })
    }

    function loadListenerPut() {
        const putbtn = document.getElementById("replacebtn");
        putbtn.addEventListener("click", function () {
            let form = document.getElementById("replace")
            let putxhttp = new XMLHttpRequest();
            putxhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 201) {
                    unloadAccounts();
                    loadAccounts();
                    form.reset();
                }
            };
            putxhttp.open("PUT", "phpdb/rest.php?replace", true);
            putxhttp.setRequestHeader('Content-Type','application/x-www-form-urlencoded')
            putxhttp.send(urlEncodeFormData(new FormData(form)));
            generateReplaceID();
        })
    }
    function urlEncodeFormData(fd){
        let s = '';
        function encode(s){ return encodeURIComponent(s).replace(/%20/g,'+'); }
        for(let pair of fd.entries()){
            if(typeof pair[1]=='string'){
                s += (s?'&':'') + encode(pair[0])+'='+encode(pair[1]);
            }
        }
        return s;
    }

    function loadAccounts() {
        let getxhttp = new XMLHttpRequest();
        getxhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                let jsondata = JSON.parse(this.responseText);
                for (let i in jsondata){
                    accounts[i] = new AccountClass();
                    Object.assign(accounts[i], jsondata[i]);
                    accounts[i].addAccountToList()
                }
                loadListenerDelete();
                generateReplaceID();
                setTimeout(function(){ setAccountAmount(); }, 200);
            }
        };
        getxhttp.open("GET", "phpdb/rest.php?info",true);
        getxhttp.send();
    }

    function unloadAccounts() {
        let x = document.querySelectorAll(".accountrows");
        x.forEach((element)=>{
            element.remove();
        })
    }

    function generateReplaceID() {
        document.forms[2].elements.namedItem("aid").innerHTML = "";

        let allAccIDs = document.querySelectorAll(".accountrows")
        for (let i = 0; i < allAccIDs.length; i++) {
            let accID = allAccIDs[i].getAttribute("id").split("account_nr_")[1]
            let select = document.forms[2].elements.namedItem("aid")
            let option = document.createElement("option")
            option.setAttribute("value", accID)
            option.appendChild(document.createTextNode(accID))
            select.appendChild(option)
        }
    }

    function setAccountAmount() {
        let table = document.getElementById("categorylist")
        for (let i = 1; i < table.rows.length; i++){
            let amount = 0;
            let tocheck = document.querySelectorAll(".accountrows")
            switch (i) {
                case 1:
                    tocheck.forEach((element)=>{
                        let value = element.getElementsByTagName("td")[4].innerHTML
                        if (value == i){
                            amount = amount +1;
                        }
                    })
                    table.rows[i].cells[2].innerHTML = ""+amount;
                    amount = 0;
                    break;
                case 2:
                    tocheck.forEach((element)=>{
                        let value = element.getElementsByTagName("td")[4].innerHTML
                        if (value == i){
                            amount = amount +1;
                        }
                    })
                    table.rows[i].cells[2].innerHTML = ""+amount;
                    amount = 0;
                    break;
                case 3:
                    tocheck.forEach((element)=>{
                        let value = element.getElementsByTagName("td")[4].innerHTML
                        if (value == i){
                            amount = amount +1;
                        }
                    })
                    table.rows[i].cells[2].innerHTML = ""+amount;
                    amount = 0;
                    break;
                case 4:
                    tocheck.forEach((element)=>{
                        let value = element.getElementsByTagName("td")[4].innerHTML
                        if (value == i){
                            amount = amount +1;
                        }
                    })
                    table.rows[i].cells[2].innerHTML = ""+amount;
                    amount = 0;
                    break;
                case 5:
                    tocheck.forEach((element)=>{
                        let value = element.getElementsByTagName("td")[4].innerHTML
                        if (value == i){
                            amount = amount +1;
                        }
                    })
                    table.rows[i].cells[2].innerHTML = ""+amount;
                    amount = 0;
                    break;
                case 6:
                    tocheck.forEach((element)=>{
                        let value = element.getElementsByTagName("td")[4].innerHTML
                        if (value == i){
                            amount = amount +1;
                        }
                    })
                    table.rows[i].cells[2].innerHTML = ""+amount;
                    amount = 0;
                    break;
            }
        }
    }
});