
# Shareables

(This is just a FAKE MOCKUP account-sharing website, which was made for a university project)

Shareables is a service which brings the whole world of entertainment right to your table!  
Shareables grants you access to various multimedia accounts including: Steam, Spotify, Netflix and much more!
## Features

- Wide collection of accounts
- Game availability checker
- Movie availability checker
- Music availability checker
- COVID-19 tracker (why not)
- Material Design
## Site

### Landing Page

![Landing1](https://imgur.com/5Bga3Tu.png)
---
![Landing2](https://imgur.com/ixhrRh7.png)

### Home Page
![home](https://imgur.com/I7RPfi6.png)

### Info Page
 ![info](https://imgur.com/sw35yQr.png)

## Built With

* [PHP](https://www.php.net/)
* [MySQL](https://www.mysql.com/)
* [Sass](https://sass-lang.com/)
* [Bootstrap](https://getbootstrap.com/)
* [PHPStorm](https://www.jetbrains.com/phpstorm/)
* [Material Design](https://material.io/design)
* passion?

## Acknowledgements

* [FH-Campus Wien](https://www.fh-campuswien.ac.at/)
* [Stackoverflow](https://stackoverflow.com/)
* [W3Schools](https://www.w3schools.com/)
