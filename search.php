<?php
include 'phpdb/admin_proof.php';
if(isset($_POST["logout"])) {
    include ('user/logout.php');
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Search</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="css/style.min.css" rel="stylesheet">

    <link href="css/main.css" rel="stylesheet">
</head>

<body>

<!--Main Navigation-->
<header>

    <!-- Navbar -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
        <div class="container">

            <!-- Brand -->
            <a class="navbar-brand waves-effect" href="index.php">
                <strong class="blue-text">Shareables</strong>
            </a>

            <!-- Collapse -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <!-- Links -->
            <div class="collapse navbar-collapse" id="navbarSupportedContent">

                <!-- Left -->
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link waves-effect" href="home.php">Home</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link waves-effect" href="search.php">Search</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link waves-effect" href="accounts.php">Your Accounts</a>
                    </li>
                    <?php
                    if($emailc['email'] == "admin@shareables.at") {?>
                        <li class="nav-item">
                            <a class="nav-link waves-effect" href="info.php">Admin Info</a>
                        </li>
                    <?php }?>
                </ul>

                <!-- Right -->
                <ul class="navbar-nav nav-flex-icons">
                    <li class="nav-item">
                        <a href="https://gitlab.com/studibrudis2020/shareables" class="nav-link waves-effect"
                           target="_blank">
                            <i class="fab fa-gitlab mr-2"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <form action="?logout" method="post">
                            <button class="nav-link border border-light rounded waves-effect" type="submit" name="logout">
                                <i class="fas fa-sign-out-alt"></i>Logout
                            </button>
                        </form>
                    </li>
                </ul>

            </div>

        </div>
    </nav>
    <!-- Navbar -->

</header>
<!--Main Navigation-->

<!--Main layout-->
<main class="mt-5 pt-5">
    <div class="container">

        <!--Section: Jumbotron-->
        <section class="card wow fadeIn" id="fancybackground">

            <!-- Content -->
            <div class="card-body text-white text-center py-5 px-5 my-5">
                <h1 class="mb-4">
                    <strong>Movie / Song / Game Availability-Checker</strong>
                </h1>
                <form id="searchform">
                    <div class="input-group input-group-lg">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-lg">Suche</span>
                        </div>
                        <input type="search" name="s" id="search"
                            <?php if (isset($_GET["s"])) echo "value='" . $_GET["s"] . "'"; ?>
                               class="form-control" aria-label="Sizing example input">
                    </div>
                </form>
            </div>
            <!-- Content -->
        </section>
        <!--Section: Jumbotron-->

        <hr class="my-5">

        <!--Section: Cards-->
        <section class="text-center">
            <h4 class="card-title">Search Results:</h4>
            <!--Grid row-->
            <div class="row mb-4 wow fadeIn" id="searchresults">

            </div>
        </section>
        <!--Section: Cards-->

    </div>
</main>
<!--Main layout-->

<!--Footer-->
<footer class="page-footer text-center font-small mt-4 wow fadeIn">

    <hr class="my-4">

    <!-- Social icons -->
    <div class="pb-4">
        <a href="about_us.php">
            <i class="fas fa-user-astronaut"></i>
        </a>
        &nbsp;&nbsp;
        <a href="https://gitlab.com/studibrudis2020/shareables" target="_blank">
            <i class="fab fa-gitlab mr-2"></i>
        </a>

    </div>
    <!-- Social icons -->

    <!--Copyright-->
    <div class="footer-copyright py-3">
        © 2020 Copyright:
        <a href="https://mdbootstrap.com" target="_blank"> MDBootstrap.com </a>
    </div>
    <!--/.Copyright-->

</footer>
<!--/.Footer-->

<!-- SCRIPTS -->
<!-- JQuery -->
<script src="js/jquery-3.4.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script src="js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script src="js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script src="js/mdb.min.js"></script>
<!-- Initializations -->
<script>
    // Animations initialization
    new WOW().init();
</script>

<script>
    $(function () {
        $("#searchform").submit(function(e) {
            e.preventDefault();

            let searchString = $("#search").val();
            $("#searchresults").empty();
            $.getJSON("https://api.rawg.io/api/games",
                {search: searchString},
                function (data) {
                    if (data.results.length != 0) {
                        for (const game of data.results) {
                            let column = $('<div>', {class: "col-lg-4 col-md-12 mb-4"});
                            let card = $('<div>', {class: "card"});
                            card.append($('<img>', {class: "card-img-top", src: game.background_image}));
                            let cardBody = $('<div>', {class: "card-body"});
                            cardBody.append($('<h5>', {text: game.name, class:"card-title"}));

                            if (game.platforms) {
                                $('<div>', {class: "card-header", text: "Platforms"}).appendTo(cardBody);
                                let platformList = $('<ul>', {class: "list-group list-group-flush"});
                                for(const p of game.platforms) {
                                    $('<li>', {text: p.platform.name, class: "list-group-item"}).appendTo(platformList);
                                }
                                cardBody.append(platformList);
                            }

                            if (game.stores) {
                                $('<div>', {class: "card-header", text: "Stores"}).appendTo(cardBody);
                                let storeList = $('<ul>', {class: "list-group list-group-flush"});
                                for(const s of game.stores) {
                                    $('<p>', {text: s.store.name, class: "list-group-item"}).appendTo(storeList);
                                }
                                cardBody.append(storeList);
                            }

                            card.append(cardBody);
                            column.append(card);
                            $("#searchresults").append(column);
                        }

                        var parent = $("#searchresults");
                        var divs = parent.children();
                        while (divs.length) {
                            parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
                        }
                    }
                }
            );

            $.getJSON("https://cors-anywhere.herokuapp.com/https://api.deezer.com/search",
                {q: searchString},
                function (data) {
                    if (data.data.length != 0) {
                        for (const track of data.data) {
                            let column = $('<div>', {class: "col-lg-4 col-md-12 mb-4"});
                            let card = $('<div>', {class: "card"});
                            card.append($('<img>', {class: "card-img-top", src: track.album.cover_xl || track.album.cover}));
                            let cardBody = $('<div>', {class: "card-body"});
                            cardBody.append($('<h5>', {text: track.title, class:"card-title"}));

                            // var p = document.createElement("p");
                            // p.innerText = track.artist.name;
                            // cardBody.appendChild(p);
                            $('<p>', {text: track.artist.name}).appendTo(cardBody);
                            $('<audio>', {src: track.preview, controls: true}).appendTo(cardBody);


                            card.append(cardBody);
                            column.append(card);
                            $("#searchresults").append(column);
                        }

                        var parent = $("#searchresults");
                        var divs = parent.children();
                        while (divs.length) {
                            parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
                        }
                    }
                }
            );



            let providers = [null,null,"Apple iTunes","Google Play Movies",null,null,"Maxdome",null,"Netflix","Amazon Prime Video","Amazon Video","Mubi",null,null,"realeyz",null,null,null,null,null,"maxdome Store",null,null,null,null,null,null,null,"Netzkino","Sky Go","Sky Ticket",null,null,"Alleskino",null,"Rakuten TV",null,null,null,null,"Chili",null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,"Microsoft Store",null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,"Kividoo",null,null,null,null,null,null,null,null,null,"Shudder","GuideDoc",null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,"Sky Store",null,null,"Videobuster",null,null,null,null,null,null,null,null,"Flimmit",null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,"Netflix Kids",null,"Pantaflix","EntertainTV",null,null,null,null,null,null,null,null,null,"YouTube Premium",null,null,null,"YouTube",null,"Starz Play Amazon Channel","Animax Plus Amazon Channel",null,null,null,null,null,"Mubi Amazon Channel",null,null,"Shudder Amazon Channel",null,null,null,null,null,null,null,null,null,null,null,null,null,null,"Das Erste Mediathek",null,null,null,null,null,null,null,null,null,null,null,null,null,null,"Arte",null,null,null,null,"Universcine",null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,"Crunchyroll",null,"BBC Player Amazon Channel","ZDF Herzkino Amazon Channel",null,null,null,null,null,null,null,null,null,null,null,"TV Now",null,null,null,null,null,"Joyn",null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,"Anime On Demand",null,null,null,null,null,null,"Filmtastic Amazon Channel",null,null,"Disney Plus",null,null,null,"blutv",null,null,"Rakuten Viki",null,null,null,null,"Kino on Demand","Apple TV Plus",null,null,null,"WAKANIM"];

            $.ajax({
                url: "https://apis.justwatch.com/content/titles/de_DE/popular",
                data: JSON.stringify({query: searchString}),
                dataType: "json",
                type: "POST",
                success: function (data) {
                    console.log(data);
                    if (data.items.length != 0) {
                        for (const movie of data.items) {
                            console.log(movie);
                            let column = $('<div>', {class: "col-lg-4 col-md-12 mb-4"});
                            let card = $('<div>', {class: "card"});
                            card.append(
                                $('<img>', {
                                    class: "card-img-top",
                                    src: "https://images.justwatch.com" + movie.poster.replace("{profile}", "s592")
                                })
                            );
                            let cardBody = $('<div>', {class: "card-body"});
                            cardBody.append($('<h5>', {text: movie.title, class:"card-title"}));

                            if(movie.offers) {
                                let prov = [];
                                for(const provider of movie.offers) {
                                    prov.push(providers[provider.provider_id]);
                                }

                                prov = prov.filter((item, index) => prov.indexOf(item) === index);

                                $('<div>', {class: "card-header", text: "Provider"}).appendTo(cardBody);
                                let providerList = $('<ul>', {class: "list-group list-group-flush"});
                                for (const p of prov) {
                                    $('<li>', {text: p, class: "list-group-item"}).appendTo(providerList);
                                }
                                cardBody.append(providerList);
                            }

                            card.append(cardBody);
                            column.append(card);
                            $("#searchresults").append(column);
                        }

                        var parent = $("#searchresults");
                        var divs = parent.children();
                        while (divs.length) {
                            parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
                        }
                    }
                }
            });
        });
    });
</script>
<!--Grid column-->
<div class="col-lg-4 col-md-12 mb-4" >
</div>
<!--Grid column-->
<script src="js/covid.js"></script>
</body>
</html>