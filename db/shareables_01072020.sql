-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 01. Jul 2020 um 16:47
-- Server-Version: 10.4.11-MariaDB
-- PHP-Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `shareables`
--
DROP DATABASE IF EXISTS `shareables`;
CREATE DATABASE IF NOT EXISTS `shareables` DEFAULT CHARACTER SET latin1 COLLATE latin1_general_ci;
USE `shareables`;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `accounts`
--

CREATE TABLE `accounts` (
  `aid` int(10) NOT NULL,
  `usermail` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `username` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `passwort` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `pid` int(3) DEFAULT NULL,
  `email` varchar(255) COLLATE latin1_general_ci DEFAULT NULL COMMENT 'User Mail zur Überprüfung ob schon zugeordnet'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Daten für Tabelle `accounts`
--

INSERT INTO `accounts` (`aid`, `usermail`, `username`, `passwort`, `pid`, `email`) VALUES
(1, 'test@netflix.com', NULL, 'test123', 1, NULL),
(2, 'test@netflix.com', NULL, 'test123', 1, NULL),
(3, 'test@netflix.com', NULL, 'test123', 1, NULL),
(4, 'anna.bolika@test.com', 'anna.bolika', 'anna1234', 2, NULL),
(5, 'phpistcool@php.com', 'php4life', 'php12345', 2, NULL),
(6, 'phpistcool@php.com', 'php4life', 'php12345', 2, NULL),
(7, NULL, 'echtergangster1', 'Fanboy1999!', 3, NULL),
(8, NULL, 'echtergangster1', 'Fanboy1999!', 3, NULL),
(9, 'daswarsmit@semester2.at', 'NeinZuCorona2020', 'CSDC22VZ', 6, NULL),
(10, 'daswarsmit@semester2.at', 'NeinZuCorona2020', 'CSDC22VZ', 6, NULL),
(11, 'daswarsmit@semester2.at', 'NeinZuCorona2020', 'CSDC22VZ', 4, NULL),
(12, 'weilwegencorona@amazonstreams.com', NULL, 'IchLadeEuchEin2020', 5, NULL),
(13, 'weilwegencorona@amazonstreams.com', NULL, 'IchLadeEuchEin2020', 5, NULL),
(14, 'weilwegencorona@amazonstreams.com', NULL, 'IchLadeEuchEin2020', 5, NULL),
(15, 'weilwegencorona@amazonstreams.com', NULL, 'IchLadeEuchEin2020', 5, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `products`
--

CREATE TABLE `products` (
  `pid` int(3) NOT NULL,
  `name` varchar(255) COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Daten für Tabelle `products`
--

INSERT INTO `products` (`pid`, `name`) VALUES
(1, 'Netflix'),
(2, 'Spotify'),
(3, 'Steam'),
(4, 'Epic Games'),
(5, 'Prime Video'),
(6, 'Origin');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `users`
--

CREATE TABLE `users` (
  `id` int(4) NOT NULL,
  `email` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `passwort` varchar(255) COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Daten für Tabelle `users`
--

INSERT INTO `users` (`id`, `email`, `passwort`) VALUES
(1, 'admin@shareables.at', '$2y$10$ZCNGjpesuY8cJMmeSFDWYuO83eJv/xL/ay1GJYRouKuP1w.4GIC.G'),
(3, 'daniel.share@shareables.at', '$2y$10$X2Z.xXbuxgTOZ60VzkxEku7eUkdRgfa4TEi1uWzIn550g2ZB/1MDK');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`aid`),
  ADD KEY `accounts_fk_email` (`email`),
  ADD KEY `accounts_fk_pid` (`pid`);

--
-- Indizes für die Tabelle `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`pid`);

--
-- Indizes für die Tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `accounts`
--
ALTER TABLE `accounts`
  MODIFY `aid` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT für Tabelle `products`
--
ALTER TABLE `products`
  MODIFY `pid` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT für Tabelle `users`
--
ALTER TABLE `users`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `accounts`
--
ALTER TABLE `accounts`
  ADD CONSTRAINT `accounts_fk_email` FOREIGN KEY (`email`) REFERENCES `users` (`email`),
  ADD CONSTRAINT `accounts_fk_pid` FOREIGN KEY (`pid`) REFERENCES `products` (`pid`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
