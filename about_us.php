<?php
include ('user/login.php');


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>About Us</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="css/style.min.css" rel="stylesheet">
    <style>
        html,
        body,
        header,
        .view {
            height: 100%;
        }

        @media (max-width: 740px) {
            html,
            body,
            header,
            .view {
                height: 1000px;
            }
        }

        @media (min-width: 800px) and (max-width: 850px) {
            html,
            body,
            header,
            .view {
                height: 650px;
            }
        }
        @media (min-width: 800px) and (max-width: 850px) {
            .navbar:not(.top-nav-collapse) {
                background: #1C2331!important;
            }
        }
    </style>
</head>

<body>

<!-- Navbar -->
<nav class="navbar fixed-top navbar-expand-lg navbar-dark scrolling-navbar">
    <div class="container">

        <!-- Brand -->
        <a class="navbar-brand" href="index.php">
            <strong>Shareables</strong>
        </a>

        <!-- Collapse -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Links -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <!-- Left -->
            <ul class="navbar-nav mr-auto">
                <!--
                    <a class="nav-link" href="">Landing
                        <span class="sr-only">(current)</span>
                    </a>
                    -->
            </ul>

            <!-- Right -->
            <ul class="navbar-nav nav-flex-icons">
                <li class="nav-item">
                    <a href="about_us.php" class="nav-link border border-light rounded">
                        <i class="fas fa-user-astronaut"></i> About us
                    </a>
                </li>
                <li class="nav-item">
                    <a href="https://gitlab.com/studibrudis2020/shareables" class="nav-link border border-light rounded"
                       target="_blank">
                        <i class="fab fa-gitlab mr-2"></i>Shareables GitLab
                    </a>
                </li>
            </ul>

        </div>

    </div>
</nav>
<!-- Navbar -->

<!--Main layout-->
<main>
    <div class="container">

        <!--Section: Main info-->
        <section class="mt-5 wow fadeIn">

            <!--Grid row-->
            <div class="row">

                <!--Grid column-->
                <div class="col-md-6 mb-4">

                    <!-- Main heading -->
                    <br><br><br>
                    <h3 class="h3 mb-3">Hello everyone!</h3>
                    <p>Shareables is a service which brings the whole world of entertainment right to your table!
                        We at Shareables believe that everyone should have access to the "good stuff"!;-)</p>
                    <br>
                    <p>So with the "good stuff" we mean accounts that we share to you for free. Yeah, totally right!
                    There is no cost for you! With our wide variety of accounts you can then have access
                    to pages like Netflix, Amazon Prime, Epic, Steam, Origin, Spotify and a whole lot more!</p>
                    <!-- Main heading -->
                    <!-- CTA -->

                </div>
                <!--Grid column-->

                <!--Grid column-->
                <div class="col-md-6 mb-4">

                    <br><br><br>
                    <img src="img/store_home_share.jpg" class="img-fluid z-depth-1-half" alt="">

                </div>
                <!--Grid column-->

            </div>
            <!--Grid row-->

            <!--Grid row-->
            <div class="row">

                <!--Grid column-->
                <div class="col-md-6 mb-4">

                    <br><br><br>
                    <img src="img/our_story.jpg" class="img-fluid z-depth-1-half" alt="">

                </div>
                <!--Grid column-->

                <!--Grid column-->
                <div class="col-md-6 mb-4">

                    <!-- Main heading -->
                    <br><br><br>
                    <h3 class="h3 mb-3">The story behind Shareables</h3>
                    <p>Once upon a time ... No, just kidding! ;-) Our story started with a school Project at
                    our FH Campus Wien. We are students at the moment and are having quite a fun time
                    designing and implementing this Website. We thought, what would be a good idea as
                    a school project and after some time of thinking and weighing the pros and cons, we
                    decided to implement: "Shareables" for you! ;-)</p>
                    <!-- Main heading -->
                    <!-- CTA -->

                </div>
                <!--Grid column-->

            </div>
            <!--Grid row-->

            <!--Grid row-->
            <div class="row">

                <!--Grid column-->
                <div class="col-md-6 mb-4">

                    <!-- Main heading -->
                    <br><br><br>
                    <h3 class="h3 mb-3">We, as a team:</h3>
                    <p>Our team consists of three teammembers:</p>
                    <ul>
                        <li>Jonathan Dimmel-Sengmüller</li>
                        <li>Daniel Hrdlicka</li>
                        <li>Raffael Tomesek</li>
                    </ul>
                    <!-- Main heading -->
                    <!-- CTA -->

                </div>
                <!--Grid column-->

                <!--Grid column-->
                <div class="col-md-6 mb-4">

                    <br><br><br>
                    <img src="img/collage.jpg" class="img-fluid z-depth-1-half" alt="">

                </div>
                <!--Grid column-->

            </div>
            <!--Grid row-->

        </section>
        <!--Section: Main info-->

        <hr class="my-5">

        <!--Section: Not enough-->
        <section>

            <h2 class="my-5 h3 text-center">You want it? You get it!</h2>

            <!--First row-->
            <div class="row features-small mb-5 mt-3 wow fadeIn">

                <!--First column-->
                <div class="col-md-4">
                    <!--First row-->
                    <div class="row">
                        <div class="col-2">
                            <i class="fas fa-check-circle fa-2x indigo-text"></i>
                        </div>
                        <div class="col-10">
                            <h6 class="feature-title">Spotify</h6>
                            <p class="grey-text">Are your neighbours too loud again? Now you can be too! Blast your favorite artists on full volume with <strong>Spotify</strong>!
                            </p>
                            <div style="height:15px"></div>
                        </div>
                    </div>
                    <!--/First row-->

                    <!--Second row-->
                    <div class="row">
                        <div class="col-2">
                            <i class="fas fa-check-circle fa-2x indigo-text"></i>
                        </div>
                        <div class="col-10">
                            <h6 class="feature-title">Netflix</h6>
                            <p class="grey-text">Wanna' movies and chill? Oh wait, it's called "<strong>Netflix</strong> and Chill"!

                            </p>
                            <div style="height:15px"></div>
                        </div>
                    </div>
                    <!--/Second row-->

                    <!--Third row-->
                    <div class="row">
                        <div class="col-2">
                            <i class="fas fa-check-circle fa-2x indigo-text"></i>
                        </div>
                        <div class="col-10">
                            <h6 class="feature-title">Prime Video</h6>
                            <p class="grey-text">Do you think that you only need one video-streaming service? You are wrong! There's <strong>Prime Video</strong> too!</p>
                            <div style="height:15px"></div>
                        </div>
                    </div>
                    <!--/Third row-->
                </div>
                <!--/First column-->

                <!--Second column-->
                <div class="col-md-4 flex-center">
                    <img src="img/landingpage2.png" alt="landingpage2" class="z-depth-0 img-fluid">
                </div>
                <!--/Second column-->

                <!--Third column-->
                <div class="col-md-4 mt-2">
                    <!--First row-->
                    <div class="row">
                        <div class="col-2">
                            <i class="fas fa-check-circle fa-2x indigo-text"></i>
                        </div>
                        <div class="col-10">
                            <h6 class="feature-title">Epic Games</h6>
                            <p class="grey-text">Well, it ain't Steam, but it got a lot of exclusive video-game titles!
                            </p>
                            <div style="height:15px"></div>
                        </div>
                    </div>
                    <!--/First row-->

                    <!--Second row-->
                    <div class="row">
                        <div class="col-2">
                            <i class="fas fa-check-circle fa-2x indigo-text"></i>
                        </div>
                        <div class="col-10">
                            <h6 class="feature-title">Origin</h6>
                            <p class="grey-text">You want to play some good ol' 'Battlefield'? You can, bust just on Origin! Thanks for nothng EA!</p>
                            <div style="height:15px"></div>
                        </div>
                    </div>
                    <!--/Second row-->

                    <!--Third row-->
                    <div class="row">
                        <div class="col-2">
                            <i class="fas fa-check-circle fa-2x indigo-text"></i>
                        </div>
                        <div class="col-10">
                            <h6 class="feature-title">Steam</h6>
                            <p class="grey-text">Now this is it. This is the pinnacle of existence! Steam! It's all you need, it's all you want for gaming!
                            </p>
                            <div style="height:15px"></div>
                        </div>
                    </div>
                    <!--/Third row-->
                </div>
                <!--/Third column-->

            </div>
            <!--/First row-->

        </section>
        <!--Section: Not enough-->
    </div>
</main>
<!--Main layout-->

<!--Footer-->
<footer class="page-footer text-center font-small mt-4 wow fadeIn">

    <hr class="my-4">

    <!-- Social icons -->
    <div class="pb-4">
        <a href="about_us.php">
            <i class="fas fa-user-astronaut"></i>
        </a>
        <a href="https://gitlab.com/studibrudis2020/shareables" target="_blank">
            <i class="fab fa-gitlab mr-2"></i>
        </a>

    </div>
    <!-- Social icons -->

    <!--Copyright-->
    <div class="footer-copyright py-3">
        © 2020 Copyright:
        <a href="https://mdbootstrap.com" target="_blank"> MDBootstrap.com </a>
    </div>
    <!--/.Copyright-->

</footer>
<!--/.Footer-->

<!-- SCRIPTS -->
<!-- JQuery -->
<script src="js/jquery-3.4.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script src="js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script src="js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script src="js/mdb.min.js"></script>
<!-- Initializations -->
<script>
    // Animations initialization
    new WOW().init();
</script>
</body>

</html>