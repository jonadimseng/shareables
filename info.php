<?php
include 'phpdb/admin_proof.php';
include 'phpdb/get_xml.php';
if($emailc['email'] == "admin@shareables.at") {
    include 'phpdb/info_daten.php';
    if (isset($_POST["logout"])) {
        include('user/logout.php');
    }
    ?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Admin Info</title>
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- Material Design Bootstrap -->
        <link href="css/mdb.min.css" rel="stylesheet">
        <!-- Your custom styles (optional) -->
        <link href="css/style.min.css" rel="stylesheet">

        <link href="css/main.css" rel="stylesheet">

        <script src="js/info.js"></script>
    </head>

    <body>

    <!--Main Navigation-->
    <header>

        <!-- Navbar -->
        <nav class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
            <div class="container">

                <!-- Brand -->
                <a class="navbar-brand waves-effect" href="index.php">
                    <strong class="blue-text">Shareables</strong>
                </a>

                <!-- Collapse -->
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <!-- Links -->
                <div class="collapse navbar-collapse" id="navbarSupportedContent">

                    <!-- Left -->
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link waves-effect" href="home.php">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link waves-effect" href="search.php">Search</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link waves-effect" href="accounts.php">Your Accounts</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link waves-effect" href="info.php">Admin Info</a>
                        </li>
                    </ul>

                    <!-- Right -->
                    <ul class="navbar-nav nav-flex-icons">
                        <li class="nav-item">
                            <a href="https://gitlab.com/studibrudis2020/shareables" class="nav-link waves-effect"
                               target="_blank">
                                <i class="fab fa-gitlab mr-2"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <form action="?logout" method="post">
                                <button class="nav-link border border-light rounded waves-effect" type="submit"
                                        name="logout">
                                    <i class="fas fa-sign-out-alt"></i>Logout
                                </button>
                            </form>
                        </li>
                    </ul>

                </div>

            </div>
        </nav>
        <!-- Navbar -->

    </header>
    <!--Main Navigation-->

    <!--Main layout-->
    <main class="mt-5 pt-5">
        <div class="container">

            <!--Section: Jumbotron-->
            <section class="card wow fadeIn" id="fancybackground">

                <!-- Content -->
                <div class="card-body text-white text-center py-5 px-5 my-5">
                    <h1 class="mb-4">
                        <strong>INFO PAGE FOR DATABASE</strong>
                    </h1>
                    <p>
                        <strong>FOR ADMINISTRATIVE PURPOSES ONLY!</strong>
                    </p>
                </div>
                <!-- Content -->
            </section>
            <!--Section: Jumbotron-->

            <hr class="my-5">

            <!--Section: Cards-->
            <section class="text-center">

                <!--Grid row-->
                <div class="row mb-4 wow fadeIn">

                    <!--Grid column-->
                    <div class="col-lg-4 col-md-12 mb-4">

                        <!--Card-->
                        <div class="card">

                            <!--Card content-->
                            <div class="card-body">
                                <!--Title-->
                                <h4 class="card-title">User-List</h4>
                                <table class="table">
                                    <tr>
                                        <th>User ID</th>
                                        <th>E-Mail</th>
                                    </tr>
                                    <?php while ($usersc = $users->fetch()) {
                                        ?>
                                        <tr>
                                            <td><?php echo $usersc['id'] ?></td>
                                            <td><?php echo $usersc['email'] ?></td>
                                        </tr>
                                    <?php } ?>
                                </table>
                                <p class="card-text">
                                    <strong></strong>
                                </p>
                            </div>

                        </div>
                        <!--/.Card-->

                    </div>
                    <!--Grid column-->

                    <!--Grid column-->
                    <div class="col-lg-4 col-md-12 mb-4">

                        <!--Card-->
                        <div class="card">

                            <!--Card content-->
                            <div class="card-body">
                                <!--Title-->
                                <h4 class="card-title">Category-List + Number of accounts per category</h4>
                                <!--Text-->
                                <table class="table" id="categorylist">
                                    <tr>
                                        <th>Category ID</th>
                                        <th>Name</th>
                                        <th>Amount</th>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>Netflix</td>
                                        <td id="netflixID"></td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Spotify</td>
                                        <td id="spotifyID"></td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Steam</td>
                                        <td id="steamID"></td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>Epic Games</td>
                                        <td id="epicgamesID"></td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>Prime Video</td>
                                        <td id="primevideoID"></td>
                                    </tr>
                                    <tr>
                                        <td>6</td>
                                        <td>Origin</td>
                                        <td id="originID"></td>
                                    </tr>
                                </table>
                                <p class="card-text">
                                    <strong></strong>
                                </p>
                            </div>

                        </div>
                        <!--/.Card-->

                    </div>
                    <!--Grid column-->
                    <!--Grid column-->
                    <div class="col-lg-4 col-md-12 mb-4">

                        <!--Card-->
                        <div class="card">

                            <!--Card image-->
                            <div class="view overlay">
                                <img src="img/admin1.png" class="card-img-top" alt="">
                            </div>

                            <!--Card content-->
                            <div class="card-body" id="breathe">
                                <!--Title-->
                                <h4 class="card-title">READ ME!</h4>
                                <!--Text-->
                                <p class="card-text">Well, well, well. If it isn't the IT-Administrator of the MONTH! You my guy, are the best of the best! You can be proud of yourself! Now go and get some more accounts so we can give them to our users! GO GO GO!</p>
                            </div>

                        </div>
                        <!--/.Card-->

                    </div>
                    <!--Grid column-->
                </div>
                <!--Grid row-->

                <!--Grid row-->
                <div class="container-fluid">
                    <div class="table-responsive text-nowrap">
                        <h4 class="card-title">Account-List</h4>

                        <!--Card table-->
                        <form id="insert" name=insert action="phpdb/rest.php?insert" method="post">
                            <table class="table">
                                <tr>
                                    <th>Create Account:</th>
                                    <td>
                                        <input type="email" class="form-control" id="usermail1" name="usermail1" placeholder="Mail">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" id="username1" name="username1" placeholder="Name">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" id="passwort1" name="passwort1" placeholder="Password" required>
                                    </td>
                                    <td>
                                        <select id="pid1" class="custom-select" name="pid1">
                                            <option value="1">Netflix</option>
                                            <option value="2">Spotify</option>
                                            <option value="3">Steam</option>
                                            <option value="4">Epic Games</option>
                                            <option value="5">Prime Video</option>
                                            <option value="6">Origin</option>
                                        </select>
                                    </td>
                                    <td>
                                        <button type="button" id="insertbtn" class="nav-link border border-light rounded waves-effect">
                                            <i class="fas fa-plus"></i>
                                        </button>
                                    </td>
                                </tr>
                            </table>
                        </form>
                        <form id="replace" name=replace action="phpdb/rest.php?replace" method="post">
                            <table class="table">
                                <tr>
                                    <td>
                                        <select class="custom-select" id="aid" name="aid"></select>
                                    </td>
                                    <td>
                                        <input type="email" class="form-control" id="usermail2" name="usermail2" placeholder="Mail">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" id="username2" name="username2" placeholder="Name">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" id="passwort2" name="passwort2" placeholder="Password" required>
                                    </td>
                                    <td>
                                        <select id="pid2" class="custom-select" name="pid2">
                                            <option value="1">Netflix</option>
                                            <option value="2">Spotify</option>
                                            <option value="3">Steam</option>
                                            <option value="4">Epic Games</option>
                                            <option value="5">Prime Video</option>
                                            <option value="6">Origin</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Claimer">
                                    </td>
                                    <td>
                                        <button type="button" id="replacebtn" class="nav-link border border-light rounded waves-effect">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                    </td>
                                </tr>

                            </table>
                        </form>
                        <table class="table" id="infoaccounts">
                            <tr>
                                <th>Account ID</th>
                                <th>E-Mail</th>
                                <th>Username</th>
                                <th>Password</th>
                                <th>Product ID</th>
                                <th>Claimed by</th>
                                <th>DELETE</th>
                            </tr>
                        </table>
                    </div>
                </div>
                <!--Grid row-->

            </section>
            <!--Section: Cards-->

        </div>
    </main>
    <!--Main layout-->

    <!--Footer-->
    <footer class="page-footer text-center font-small mt-4 wow fadeIn">

        <hr class="my-4">

        <!-- Social icons -->
        <div class="pb-4">
            <a href="about_us.php">
                <i class="fas fa-user-astronaut"></i>
            </a>
            &nbsp;&nbsp;
            <a href="https://gitlab.com/studibrudis2020/shareables" target="_blank">
                <i class="fab fa-gitlab mr-2"></i>
            </a>

        </div>
        <!-- Social icons -->

        <!--Copyright-->
        <div class="footer-copyright py-3">
            © 2020 Copyright:
            <a href="https://mdbootstrap.com" target="_blank"> MDBootstrap.com </a>
        </div>
        <!--/.Copyright-->

    </footer>
    <!--/.Footer-->

    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script src="js/jquery-3.4.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script src="js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script src="js/mdb.min.js"></script>
    <!-- Initializations -->
    <script>
        // Animations initialization
        new WOW().init();
    </script>
    <script src="js/covid.js"></script>
    </body>
    </html>
    <?php
} else{
    echo '<script language="javascript">alert("You are not an administrator!")</script>';
    header('Refresh: 0; url=home.php');
}
?>